############################ - Import 
import tensorflow as tf
import sys, os
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisNew")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
from TensorFlowAnalysis import *
import pandas as pd
from root_pandas import to_root
import time
import pickle
import itertools
import tensorflow_probability as tfp
import functools
tfd = tfp.distributions
fitdir=os.path.dirname(os.path.abspath(__file__))
############################

############################ - Make constants
#Masses 
MLb     = 5619.49997776e-3    #GeV
MLc     = 2286.45992749e-3    #GeV
Mlep    = 105.6583712e-3     #GeV Mu
#Mlep    = 1.77686             #GeV Tau
#Global constants express all in GeV
GF      = Const(1.166378e-5) #GeV^-2
mb      = Const(4.18       ) #GeV
mc      = Const(1.275      ) #GeV
Vcb     = Const(4.22e-2    ) #avg of incl and excl
#derived constants
Mpos    = Const(MLb) + Const(MLc)
Mneg    = Const(MLb) - Const(MLc)
t0      = Pow(Mneg,2)
sqrttwo = Sqrt(np.array(2.)) #Used a alot and complains about tf.dtype, so define it here with np
#For FF parametrisation
Tf_plus = {} #mf_pole = M_BC + delta_f and tf_plus = mf_pole**2
Tf_plus['fplus']      = Pow(Const(6.276 + 56e-3         ), 2)#GeV
Tf_plus['fperp']      = Pow(Const(6.276 + 56e-3         ), 2)#GeV
Tf_plus['f0']         = Pow(Const(6.276 + 449e-3        ), 2)#GeV
Tf_plus['gplus']      = Pow(Const(6.276 + 492e-3        ), 2)#GeV
Tf_plus['gperp']      = Pow(Const(6.276 + 492e-3        ), 2)#GeV
Tf_plus['g0']         = Pow(Const(6.276 + 0.            ), 2)#GeV
Tf_plus['hplus']      = Pow(Const(6.276 + 6.332 - 6.276 ), 2)#GeV
Tf_plus['hperp']      = Pow(Const(6.276 + 6.332 - 6.276 ), 2)#GeV
Tf_plus['htildeplus'] = Pow(Const(6.276 + 6.768 - 6.276 ), 2)#GeV
Tf_plus['htildeperp'] = Pow(Const(6.276 + 6.768 - 6.276 ), 2)#GeV
#Define signs
eta    = {'t': 1.,  0 :-1.,  -1: -1., 1 : -1.}
signWC = {'V': 1., 'A':-1., 'S': 1.,'PS': -1., 'T': 1., 'PT':-1.}
#defile helicity list (NB: To build Lc amplitude defined everything in units of 1/2 except for lw used in leptonic case that does not need this)
#incoherent hel
lLbs = [1, -1]
lLcs = [1, -1]
lls  = [1, -1]
#coherent hel
lws  = ['t', 0, 1, -1]
WCs  = ['V', 'A', 'S', 'PS', 'T', 'PT']
FFs  = ['a0f0', 'a0fplus', 'a0fperp', 'a0g0', 'a0gplus', 'a0hplus', 'a0hperp', 'a0htildeplus']
FFs += ['a1f0', 'a1fplus', 'a1fperp', 'a1g0', 'a1gplus', 'a1gperp', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
FFs += ['a0gperp', 'a0htildeperp']
ffactSM  = {}
f     = open(fitdir+"/FF_cov/LambdabLambdac_results.dat", "r")
for l in f.readlines(): ffactSM[l.split()[0]] = float(l.split()[1])
ffactSM['a0gperp']      = ffactSM['a0gplus']
ffactSM['a0htildeperp'] = ffactSM['a0htildeplus']
f.close()
#if float the ff needed to be floated too
map_wc_ff = {}
map_wc_ff['CVL']  = ['a0f0', 'a0fplus', 'a0fperp', 'a1f0', 'a1fplus', 'a1fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a1g0', 'a1gplus', 'a1gperp']
map_wc_ff['CVR']  = ['a0f0', 'a0fplus', 'a0fperp', 'a1f0', 'a1fplus', 'a1fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a1g0', 'a1gplus', 'a1gperp']
map_wc_ff['CSL']  = ['a0f0', 'a1f0', 'a0g0', 'a1g0']
map_wc_ff['CSR']  = ['a0f0', 'a1f0', 'a0g0', 'a1g0']
map_wc_ff['CT']   = ['a0hplus', 'a0hperp', 'a0htildeperp', 'a0htildeplus', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
############################

########### some utilities
def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
###########

############################ - define functions
def get_wilsonparams():
    cvl = FitParameter("CVL" , 0.0, -2., 2., 0.08)
    cvr = FitParameter("CVR" , 0.0, -2., 2., 0.08)
    csr = FitParameter("CSR" , 0.0, -2., 2., 0.08)
    csl = FitParameter("CSL" , 0.0, -2., 2., 0.08)
    ct  = FitParameter("CT"  , 0.0, -2., 2., 0.08)
    Wlcoef = {}
    Wlcoef['V' ]  = (1. + cvl + cvr) 
    Wlcoef['A' ]  = (1. + cvl - cvr) 
    Wlcoef['S' ]  = (csl+ csr)
    Wlcoef['PS']  = (csl- csr)
    Wlcoef['T' ]  = ct
    Wlcoef['PT']  = ct
    return Wlcoef

def get_ffparams():
    ffact = {}
    #make ff
    for FF in FFs[:-2]: 
        print('Setting', FF, 'to SM value:', ffactSM[FF])
        ffact[FF] = FitParameter(FF, ffactSM[FF], ffactSM[FF]-2., ffactSM[FF]+2., 0.08)
        ffact[FF].fix()
        
    ffact['a0gperp']      = ffact['a0gplus']
    ffact['a0htildeperp'] = ffact['a0htildeplus']
    for ff in map_wc_ff['CVL']: 
        print('Floating FF for SM:', ff)
        ffact[ff].float()

    return ffact

def get_freeparams_dict(Wilcofparam, fFactparam):
    #WC considered real and make freeparams
    freeparam = {}; Switches  = {}
    for WC in WCs: 
        for FF in FFs:
            freeparam[( WC, FF)] = Wilcofparam[WC] * fFactparam[FF]
            Switches[(  WC, FF)] = tf.placeholder_with_default([CastReal(1.)], shape=(1,))

    #At this point only wc are floated + the ones related to SM (where CVL=0)
    wc_float = [p.par_name for p in tf.trainable_variables() if p.floating() if 'C' in p.par_name] 
    for wcf in wc_float:
        for ff in map_wc_ff[wcf]: 
            if wcf == 'CT':
                print('For the floated WC:', wcf, 'floating FF:', ff)
                fFactparam[ff].float()

    return freeparam, Switches

def MakeFourVector(pmag, costheta, phi, msq): 
    sintheta = Sqrt(1 - costheta**2) #theta 0 to pi => Sin always pos
    px = pmag * sintheta * Cos(phi)
    py = pmag * sintheta * Sin(phi)
    pz = pmag * costheta
    E  = Sqrt(msq + pmag**2)
    return LorentzVector(Vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): #Mag of (1 or 2) in M rest frame
    kallen = Sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*Sqrt(Msq))

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    """
    #First:  Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame (i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = RotateLorentzVector(m_p4mom_p1, -gm_phi_m, Acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = RotateLorentzVector(m_p4mom_p2, -gm_phi_m, Acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = BoostFromRest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = BoostFromRest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    """
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = RotateLorentzVector(BoostToRest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = RotateLorentzVector(lc_p4mom_p, -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = RotateLorentzVector(lc_p4mom_k, -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = BoostToRest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = BoostToRest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    return gm_p4mom_p1, gm_p4mom_p2

def Prepare_data(x):
    Vars = {}
    #Store Lc phsp Varsiables and make sure volume element stays as dq2*dcthlc*dcthl*dphl*dm2pk
    q2            =  x[:,0]
    w_ctheta_l    = -x[:,1] #This because in MC it is like this
    lb_ctheta_lc  = Ones(x[:,0])  #Lb polarisation is zero (shape same as integrating or fixing)
    lb_phi_lc     = Zeros(x[:,0]) #always zeros
    w_phi_l       = Zeros(x[:,0]) #Lb polarisation is zero (shape same as integrating or fixing)

    #Store info of other particles
    lb_ctheta_w   = -lb_ctheta_lc
    lb_phi_w      =  lb_phi_lc+Pi()
    w_ctheta_nu   = -w_ctheta_l
    w_phi_nu      =  w_phi_l+Pi()

    #Lc and W 4mom in Lb rest frame     (zLb = pbeam_lab x pLb_lab, yLb = zLb x lb_p3vec_lc, x = y x z => r, theta, phi = lb_p3mag_lc,lb_ctheta_lc, 0)
    lb_p3mag_lc  = pvecMag(Const(MLb)**2, Const(MLc)**2, q2)
    lb_p4mom_lc  = MakeFourVector(lb_p3mag_lc,     lb_ctheta_lc, lb_phi_lc, Const(MLc)**2)
    lb_p4mom_w   = MakeFourVector(lb_p3mag_lc,     lb_ctheta_w,  lb_phi_w , q2)
    #l and nu 4mom in W helicity frame (zw = lb_p3vec_w, yw = yLb = zLb x lb_p3vec_lc , xw = yw x zw => r, theta, phi => w_p3mag_l, w_ctheta_l, w_phl_l)
    w_p3mag_l    = pvecMag(q2, Const(Mlep)**2, 0.)
    w_p4mom_l    = MakeFourVector(w_p3mag_l,    w_ctheta_l , w_phi_l , Const(Mlep)**2)
    w_p4mom_nu   = MakeFourVector(w_p3mag_l,    w_ctheta_nu, w_phi_nu, 0.)

    #Get everything is Lb rest frame
    lb_p4mom_l , lb_p4mom_nu = InvRotateAndBoostFromRest(w_p4mom_l,   w_p4mom_nu, lb_phi_w,  lb_ctheta_w,  lb_p4mom_w)

    #Store Varss
    Vars['q2']            = q2
    Vars['El']            = TimeComponent(lb_p4mom_l)
    Vars['m_Lbmu']        = Mass(lb_p4mom_lc+lb_p4mom_l)
    #lnu angles
    Vars['lb_ctheta_w']   = lb_ctheta_w
    Vars['lb_phi_w']      = lb_phi_w
    Vars['w_ctheta_l']    = w_ctheta_l
    Vars['w_phi_l']       = w_phi_l
    #3mom mag 
    Vars['lb_p3mag_lc']   = lb_p3mag_lc
    Vars['w_p3mag_l']     = w_p3mag_l

    return Vars

def get_Lb_ampls(Obs):
    """
    Some important comments about the hadronic currents:
    V currents:
        - aV, bV and cV are form factor parameter independent (unlike in our paper)
        - When summing over lwd only one term is needed. This term is taken as 't' (b'cos eta[t] = 1). Make sure Leptonic V and A current lwd='t'.
        - The ffterms func return corrent q2 dependent terms pertaining to FF parameter a0 and a1.
        - Hadr(lb, lc, lw, 't', 'V', 'a0') have 16 comb out of which 12 survive and 4 are zero
    S currents:
        - Fake lw and lwd index (see above for reason). Fixed to zero, make sure Leptonic S, PS currents also have lw = lwd = 't'.
        - Hadr(lb, lc, 't', 't', 'S', 'a0') have 4 terms all which are nonzero.
    A currents: 
        - Like in Leptonic current these are NOT equal to V currents.
        - Same comments as V currents.
    PS currents: 
        - Like in Leptonic current these are NOT equal to S currents.
        - Same comments as S currents.
    T currents:
        - Hadr(lb, lc, lw, lwd, 'T', 'a0') have 64 terms out of which 32 are nonzero.
    PT currents:
        - Like in Leptonic current these are NOT equal to T currents.
        - same comments as T currents.
    Func returns:
        Dict with index as tuples Hadr[(lb, lc, lw, lwd, wc, ff)]
    """
    #q2 and angular terms: Used sin(x/2) = sqrt((1-cthlc)/2) and cos(x/2)=sqrt((1+cthlc)/2).Since x/2 ranges from 0 to pi/2 both sine and cosine are pos.
    q2          =  Obs['q2']
    sqrtQp      = Sqrt(Pow(Mpos,2) - q2)
    sqrtQn      = Sqrt(Pow(Mneg,2) - q2)
    sqrtQpMn    = sqrtQp * Mneg
    sqrtQnMp    = sqrtQn * Mpos
    aV          = sqrtQpMn/Sqrt(q2)
    bV          = sqrtQnMp/Sqrt(q2)
    cV          = sqrttwo * sqrtQn
    aA          = bV
    bA          = aV
    cA          = sqrttwo * sqrtQp
    aS          = sqrtQpMn/(mb - mc)
    aP          = sqrtQnMp/(mb + mc)
    aT          = sqrtQn
    bT          = sqrttwo * bV
    cT          = sqrttwo * aV
    dT          = sqrtQp
    cthlc       = -Obs['lb_ctheta_w'] #ONE Lb polarisation ignored
    costhlchalf = Sqrt((1+cthlc)/2.)  #One Lb polarisation ignored
    sinthlchalf = Sqrt((1-cthlc)/2.)  #ZERO Lb polarisation ignored, so commented out the terms

    #get only q2 dependent terms in FF expansion i.e. ones pertaining to a0 and a1.
    ffterms = {}
    for ff in ['fplus', 'fperp', 'f0', 'gplus', 'gperp', 'g0', 'hplus', 'hperp', 'htildeperp', 'htildeplus']:
        cf = 1./(1. - q2/Tf_plus[ff] )
        zf = (Sqrt(Tf_plus[ff] - q2) - Sqrt(Tf_plus[ff] - t0))/(Sqrt(Tf_plus[ff] - q2) + Sqrt(Tf_plus[ff] - t0))
        ffterms['a0'+ff] =  cf
        ffterms['a1'+ff] =  cf * zf

    Hadr = {}

    #f0 terms: contains V and S currents
    for a in ['a0f0', 'a1f0']:
        Hadr[( 1, 1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
        Hadr[( 1, 1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
        #Hadr[( 1,-1,'t', 't', 'V', a)] = -sinthlchalf * aV * ffterms[a]
        #Hadr[(-1, 1,'t', 't', 'V', a)] =  sinthlchalf * aV * ffterms[a]
        #Hadr[( 1,-1,'t', 't', 'S', a)] = -sinthlchalf * aS * ffterms[a]
        #Hadr[(-1, 1,'t', 't', 'S', a)] =  sinthlchalf * aS * ffterms[a]
    
    #fplus terms
    for a in ['a0fplus', 'a1fplus']:
        Hadr[( 1, 1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
        Hadr[(-1,-1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
        #Hadr[( 1,-1,  0, 't', 'V', a)] = -sinthlchalf  * bV * ffterms[a]
        #Hadr[(-1, 1,  0, 't', 'V', a)] =  sinthlchalf  * bV * ffterms[a]
    
    #fperp terms
    for a in ['a0fperp', 'a1fperp']:
        Hadr[( 1,-1, -1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
        Hadr[(-1, 1,  1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
        #Hadr[(-1,-1, -1, 't', 'V', a)] = -sinthlchalf  * cV * ffterms[a]
        #Hadr[( 1, 1,  1, 't', 'V', a)] =  sinthlchalf  * cV * ffterms[a]
    
    #g0 terms: contains A and PS currents
    for a in ['a0g0','a1g0']:
        Hadr[( 1, 1,'t', 't', 'A', a)] =  costhlchalf * aA * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'A', a)] = -costhlchalf * aA * ffterms[a]
        Hadr[( 1, 1,'t', 't','PS', a)] = -costhlchalf * aP * ffterms[a]
        Hadr[(-1,-1,'t', 't','PS', a)] =  costhlchalf * aP * ffterms[a]
        #Hadr[( 1,-1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
        #Hadr[(-1, 1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
        #Hadr[( 1,-1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
        #Hadr[(-1, 1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
    
    #gplus terms
    for a in ['a0gplus','a1gplus']:
        Hadr[( 1, 1,  0, 't', 'A', a)] =  costhlchalf * bA * ffterms[a]
        Hadr[(-1,-1,  0, 't', 'A', a)] = -costhlchalf * bA * ffterms[a]
        #Hadr[( 1,-1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
        #Hadr[(-1, 1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
    
    #gperp terms
    for a in ['a0gperp','a1gperp']:
        Hadr[( 1,-1, -1, 't', 'A', a)] =  costhlchalf * cA * ffterms[a]
        Hadr[(-1, 1,  1, 't', 'A', a)] = -costhlchalf * cA * ffterms[a]
        #Hadr[(-1,-1, -1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
        #Hadr[( 1, 1,  1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
    
    #hplus terms: T and PT
    for a in ['a0hplus','a1hplus']:
        Hadr[( 1, 1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
        Hadr[( 1, 1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
        Hadr[( 1, 1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
        Hadr[( 1, 1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
        #Hadr[( 1,-1,'t',  0, 'T', a)] = -sinthlchalf * aT * ffterms[a]
        #Hadr[(-1, 1,'t',  0, 'T', a)] =  sinthlchalf * aT * ffterms[a]
        #Hadr[( 1,-1,  0,'t', 'T', a)] =  sinthlchalf * aT * ffterms[a]
        #Hadr[(-1, 1,  0,'t', 'T', a)] = -sinthlchalf * aT * ffterms[a]
        #Hadr[( 1,-1,  1, -1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
        #Hadr[(-1, 1,  1, -1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
        #Hadr[( 1,-1, -1,  1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
        #Hadr[(-1, 1, -1,  1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
    
    #hperp terms: T and PT
    for a in ['a0hperp','a1hperp']:
        Hadr[(-1, 1,'t',  1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1,'t', -1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1, -1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1,  0, -1, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1, -1,  0, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  0,  1, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  1,  0, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
        #Hadr[(-1,-1,'t', -1, 'T', a)] = -sinthlchalf * bT *  ffterms[a]
        #Hadr[( 1, 1,'t',  1, 'T', a)] =  sinthlchalf * bT *  ffterms[a]
        #Hadr[(-1,-1, -1,'t', 'T', a)] =  sinthlchalf * bT *  ffterms[a]
        #Hadr[( 1, 1,  1,'t', 'T', a)] = -sinthlchalf * bT *  ffterms[a]
        #Hadr[(-1,-1,  0, -1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        #Hadr[( 1, 1,  1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
        #Hadr[(-1,-1, -1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
        #Hadr[( 1, 1,  0,  1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
    
    #htildeperp terms: T and PT
    for a in ['a0htildeperp','a1htildeperp']:
        Hadr[(-1, 1,  0,  1, 'T', a)] = -costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1,  0, -1, 'T', a)] = -costhlchalf * cT * ffterms[a]
        Hadr[(-1, 1,  1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1, -1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1,'t', -1, 'PT',a)] = -costhlchalf * cT * ffterms[a]
        Hadr[(-1, 1,'t',  1, 'PT',a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1, -1,'t', 'PT',a)] =  costhlchalf * cT * ffterms[a]
        Hadr[(-1, 1,  1,'t', 'PT',a)] = -costhlchalf * cT * ffterms[a]
        #Hadr[(-1,-1,  0, -1, 'T', a)] = -sinthlchalf * cT * ffterms[a]
        #Hadr[( 1, 1,  0,  1, 'T', a)] =  sinthlchalf * cT * ffterms[a]
        #Hadr[(-1,-1, -1,  0, 'T', a)] =  sinthlchalf * cT * ffterms[a]
        #Hadr[( 1, 1,  1,  0, 'T', a)] = -sinthlchalf * cT * ffterms[a]
        #Hadr[( 1, 1,'t',  1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
        #Hadr[(-1,-1,'t', -1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
        #Hadr[( 1, 1,  1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        #Hadr[(-1,-1, -1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
    
    #htildeplus terms: T and PT
    for a in ['a0htildeplus','a1htildeplus']:
        Hadr[( 1, 1,  1, -1, 'T', a)] = -costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,  1, -1, 'T', a)] =  costhlchalf * dT * ffterms[a]
        Hadr[( 1, 1, -1,  1, 'T', a)] =  costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1, -1,  1, 'T', a)] = -costhlchalf * dT * ffterms[a]
        Hadr[( 1, 1,'t',  0, 'PT',a)] = -costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,'t',  0, 'PT',a)] =  costhlchalf * dT * ffterms[a]
        Hadr[( 1, 1,  0,'t', 'PT',a)] =  costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,  0,'t', 'PT',a)] = -costhlchalf * dT * ffterms[a]
        #Hadr[( 1,-1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
        #Hadr[(-1, 1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
        #Hadr[( 1,-1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
        #Hadr[(-1, 1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
        #Hadr[( 1,-1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
        #Hadr[(-1, 1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
        #Hadr[( 1,-1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
        #Hadr[(-1, 1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]

    return Hadr

def get_W_ampls(Obs):
    """
    Some important comments on all the leptonic currents
    V or A current:
        - Terms with WC index V and A are equal.
        - Since only one term appears in the ampl for lwd. We fix lwd to 't' as done in hadronic V, A currents. Index 't' choosen because eta['t'] = 1.
        - Therefore Lept(lw, 't', 'V', ll) has total 8 components out of which 1 is zero.
    S or PS current:
        - Terms with WC index S and PS are equal.
        - In this case both lw and lwd are fake indeces (see above for reason), choosen to be 't' (see above) as done in hadronic S, PS currents.
        - Lept('t', 't', 'S', ll) has total 2 components out of which 1 is zero.
    T or PT current:
        - Terms with WC index T and PT are equal.
        - Lept(lw,lwd,'T',l) has total 32 components out of which 8 are zero.
    Func returns:
        Dict with index as tuples Lept[(lw, lwd, wc, ll)]
    """
    
    #q2 and angular terms
    q2              = Obs['q2'] #real
    cthl            = Obs['w_ctheta_l']
    phl             = Obs['w_phi_l'] #ZERO Lb polarisation ignored as a result amplitude real
    v               = Sqrt(1. - Pow(Const(Mlep),2)/q2) #real
    sinthl          = Sqrt(1.-Pow(cthl,2.))
    expPlusOneIphl  = Cos(phl)    #real and One since Lb polarisation ignored
    expMinusOneIphl = Cos(-phl)   #real and One since Lb polarisation ignored
    expMinusTwoIphl = Cos(-2.*phl)#real and One since Lb polarisation ignored
    OneMinuscthl    = (1. - cthl)
    OnePluscthl     = (1. + cthl)
    al              = 2. * Const(Mlep) * v
    bl              = 2. * Sqrt(q2)* v
    complexsqrttwo  = sqrttwo
    
    Lept = {}
    
    Lept[('t', 't', 'V',  1)] =  expMinusOneIphl * al
    Lept[(  0, 't', 'V',  1)] = -expMinusOneIphl * cthl * al
    Lept[(  1, 't', 'V',  1)] =  expMinusTwoIphl * sinthl * al/complexsqrttwo
    Lept[( -1, 't', 'V',  1)] = -sinthl * al/complexsqrttwo
    Lept[(  0, 't', 'V', -1)] =  sinthl * bl
    Lept[(  1, 't', 'V', -1)] =  expMinusOneIphl * OnePluscthl  * bl/complexsqrttwo
    Lept[( -1, 't', 'V', -1)] =  expPlusOneIphl  * OneMinuscthl * bl/complexsqrttwo
    
    Lept[('t', 't', 'S', 1)]  =  expMinusOneIphl * bl
    
    Lept[('t',  0, 'T', -1)]  =  sinthl * al
    Lept[(  1, -1, 'T', -1)]  =  sinthl * al
    Lept[(  0, -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[('t', -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[('t',  1, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  0,  1, 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[('t',  0, 'T',  1)]  = -expMinusOneIphl * cthl * bl
    Lept[(  1, -1, 'T',  1)]  = -expMinusOneIphl * cthl * bl
    Lept[('t',  1, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[(  0,  1, 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[('t', -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
    Lept[(  0, -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
    Lept[(  0,'t', 'T', -1)]  = -sinthl * al
    Lept[( -1,  1, 'T', -1)]  = -sinthl * al
    Lept[( -1,  0, 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[( -1,'t', 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[(  1,'t', 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  1,  0, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  0,'t', 'T',  1)]  =  expMinusOneIphl * cthl * bl
    Lept[( -1,  1, 'T',  1)]  =  expMinusOneIphl * cthl * bl
    Lept[(  1,'t', 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[(  1,  0, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[( -1,'t', 'T',  1)]  =  sinthl * bl/complexsqrttwo
    Lept[( -1,  0, 'T',  1)]  =  sinthl * bl/complexsqrttwo

    return Lept

def ReplaceWC(strg):
    strg_wc = strg.replace('A' , 'V')
    strg_wc = strg_wc.replace('PS', 'S')
    strg_wc = strg_wc.replace('PT', 'T')
    return strg_wc
    
def get_dens(freeparams, Lbampl, Wampl, sws, sess = None):
    Dens = Const(0.)
    for lLb in lLbs:
        for lLc in lLcs:
            for ll in lls:
                ampls_coherent = Const(0.)
                for lw in lws:
                    for lwd in lws:
                        for WC in WCs:
                            for FF in FFs:
                                lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                windx  = (lw,lwd,ReplaceWC(WC),ll)
                                fpindx = (WC, FF)
                                cond   = lbindx not in Lbampl or windx not in Wampl
                                if cond: continue
                                ampls_coherent+=sqrttwo*GF*Vcb*signWC[WC]*eta[lw]*eta[lwd]*Lbampl[lbindx]*Wampl[windx]*freeparams[fpindx]*sws[fpindx]
                                #print(sess.run(sqrttwo*GF*Vcb*signWC[WC]*eta[lw]*eta[lwd]*Lbampl[lbindx]*Wampl[windx]*freeparams[fpindx]*sws[fpindx]))

                Dens += Density(ampls_coherent)

    return Dens

def dPhiTwoBody(mijsq, misq, mjsq):
    return 1./(2.**4 * Pi()**2) * pvecMag(mijsq, misq, mjsq)/Sqrt(mijsq)

def getPhasespaceTerm(Obs):
    #d(lb_ctheta_lc) * d(q2) * d(w_ctheta_l) * d(w_phi_l)
    phsspace  = 1./(2. * Const(MLb) * 2. * Pi()) * dPhiTwoBody(Const(MLb)**2, Const(MLc)**2, Obs['q2'])   #Lb -> Lc W
    phsspace *= dPhiTwoBody(Obs['q2'], Const(Mlep)**2, 0.)   #W  ->  l nu
    return phsspace

def getDynamicTerm(obsLb, fp_wc_ff, sw_wc_ff, sess = None):
    LbDecay_lLb_lLc_lw_lwd_WC_FF = get_Lb_ampls(obsLb) 
    WDecay_lw_lwd_WC_ll          = get_W_ampls(obsLb)  
    #print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 reduces to 92 if Lb is unpolarised
    #print(len(WDecay_lw_lwd_WC_ll.keys()))             #32
    #print(len(fp_wc_ff.keys()))                        #6*20 = 120
    #print(len(sw_wc_ff.keys()))                        #6*20 = 120
    dynm = get_dens(fp_wc_ff, LbDecay_lLb_lLc_lw_lwd_WC_FF, WDecay_lw_lwd_WC_ll, sw_wc_ff, sess = sess)
    return dynm

def AngularModel(ph, fp, sw, sess = None):
    obsv              = Prepare_data(ph) 
    dGdO_phsp         = getPhasespaceTerm(obsv)
    dGdO_dynm         = getDynamicTerm(obsv, fp, sw, sess = sess)
    MoDel             = dGdO_phsp*dGdO_dynm
    return MoDel, obsv

def get_FPIndp_terms_test(ph, sess = None):
    obsv       = Prepare_data(ph) 
    dGdO_phsp  = getPhasespaceTerm(obsv)
    Lbampl     = get_Lb_ampls(obsv) 
    Wampl      = get_W_ampls(obsv)  
    
    fp_indp_terms = {}
    #THIS implementation is WAY better then tensor contraction in TF. Specially in terms of memory.
    #Also better than introducing switches since integration has to be looped and done over each term, whereas a dictionary of integral can be directly evaluated.
    for WC in WCs:
        for FF in FFs:
            for WC_d in WCs:
                for FF_d in FFs:
                    fpindx   = (WC,   FF)
                    fpindx_d = (WC_d, FF_d)
                    if (fpindx_d, fpindx) in fp_indp_terms: continue
                    dens   = Const(0.)
                    filled = False #Only store stuff that gets evaluated (i.e. actually that reaches the end of the loops below pass hurdles
                    for lLb in lLbs:
                        for lLc in lLcs:
                            for ll in lls:
                                for lw in lws:
                                    for lwd in lws:
                                        lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                        windx  = (lw,lwd,ReplaceWC(WC),ll)
                                        cond1  = lbindx not in Lbampl or windx not in Wampl
                                        if cond1: continue #hurdle1
                                        amp1  = sqrttwo*GF*Vcb*signWC[WC]*eta[lw]*eta[lwd]*Lbampl[lbindx]*Wampl[windx]
                                        for lLb_d in lLbs:
                                            for lLc_d in lLcs:
                                                for ll_d in lls:
                                                    for lw_d in lws:
                                                        for lwd_d in lws:
                                                            lbindx_d = (lLb_d,lLc_d,lw_d,lwd_d,WC_d,FF_d)
                                                            windx_d  = (lw_d,lwd_d,ReplaceWC(WC_d),ll_d)
                                                            cond2 = lbindx_d not in Lbampl or windx_d not in Wampl
                                                            cond3 = lLb_d != lLb or lLc_d != lLc or ll_d != ll
                                                            if cond2 or cond3: continue #hurdle2
                                                            amp2  = sqrttwo*GF*Vcb*signWC[WC_d]*eta[lw_d]*eta[lwd_d]*Lbampl[lbindx_d]*Wampl[windx_d]
                                                            filled = True
                                                            if fpindx == fpindx_d: 
                                                                dens += amp1 * amp2 
                                                            else:
                                                                dens += Const(2.) * amp1 * amp2 

                    if filled: fp_indp_terms[(fpindx,fpindx_d)] = tf.reduce_mean(dGdO_phsp * dens) #only store the non-zero or the ones that pass hurdels

    return fp_indp_terms

def getWeights(ph, wilcoefparam, ffacTparam, sess, data_sample, fname):
    obsLb                        = Prepare_data(ph) 
    LbDecay_lLb_lLc_lw_lwd_WC_FF = get_Lb_ampls(obsLb) 
    WDecay_lw_lwd_WC_ll          = get_W_ampls(obsLb)  
    fp_wc_ff, _                  = get_freeparams_dict(wilcoefparam, ffacTparam) 
    #print(len(fp_wc_ff.keys()))                        #6*20 = 120
    #print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 reduces to 92 if Lb is unpolarised
    #print(len(WDecay_lw_lwd_WC_ll.keys()))             #32

    #NB: The phase space terms are independent free parameters
    set_SM_vals(wilcoefparam, ffacTparam, sess)
    fd_dt       = {ph: data_sample}
    np_dynm_SM  = tf.convert_to_tensor(sess.run(get_dens(fp_wc_ff, LbDecay_lLb_lLc_lw_lwd_WC_FF, WDecay_lw_lwd_WC_ll), feed_dict=fd_dt), dtype=tf.float64)
    print(np_dynm_SM)

    Indp_dynm   = get_FP_Indp_terms(LbDecay_lLb_lLc_lw_lwd_WC_FF, WDecay_lw_lwd_WC_ll, np_dynm_SM)
    #ws_check    = contract_with_wcff(fp_wc_ff, Indp_dynm)
    np.save(fname, sess.run(Indp_dynm, feed_dict=fd_dt))
    return None

def genmodel(Sess, model, ph, Smpl, php, Seed, Size, Obsv = None, fname = None):
    majorant    = EstimateMaximum(Sess, model, ph, Smpl)*1.5
    print('Majorant', majorant)
    fit_sample  = RunToyMC(Sess, model, ph, php, Size, majorant, chunk = 700000, seed = Seed)
    if Obsv and fname:
        ditc        = Sess.run(Obsv, feed_dict = {ph: fit_sample})
        df          = pd.DataFrame.from_dict(ditc)
        to_root(df, fname, key='tree', store_index=False)

    return fit_sample

def plot_res(dt, fitres, nbins, xmin, xmax, fname, xlabel, ylabel, wght_dt = None):
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    plt.rcParams['axes.unicode_minus'] = True

    dta, bdta      = np.histogram(dt,     bins = nbins,  weights = wght_dt, range=(xmin,xmax))
    bw             = bdta[1:] - bdta[:-1]     
    bin_centers    = bdta[:-1] + 0.5 * bw
    fitres_ty, _   = np.histogram(fitres, bins = nbins,  weights = None, range=(xmin,xmax))

    fitres_ty_norm = dta.sum() * fitres_ty/fitres_ty.sum()
    nm             = dta - fitres_ty_norm
    dnm            = np.sqrt(np.sqrt(dta)**2 + np.sqrt(fitres_ty_norm)**2)
    pull           = np.divide(nm, dnm, out=np.zeros_like(nm), where=dnm!=0.)
    pull           = np.nan_to_num(pull)

    fig = plt.figure()
    gs  = gridspec.GridSpec(2, 1, height_ratios = [4,1])
    axs = plt.subplot(gs[0]), plt.subplot(gs[1])

    axs[0].errorbar(bin_centers, dta.flatten(), xerr=nbins*[0.5*(xmax-xmin)/nbins], yerr=(np.sqrt(dta)).flatten(),\
    fmt = '.', color = 'black', label='Data', linewidth = 2.0, markersize=9)
    axs[0].hist(bdta[:-1], bdta, weights=fitres_ty_norm.flatten(), label='Fit', histtype = 'step', linewidth = 2.5, range=(xmin, xmax), color='b')

    cmax = [np.max(dta), np.max(fitres_ty_norm)]
    cmin = [np.min(dta), np.min(fitres_ty_norm)]
    ymin = max(min(cmin), 1e-10); ymax = max(cmax)+1000.
    axs[0].legend(loc='best')
    axs[0].set_xlim((xmin,xmax))
    axs[0].set_xlabel(xlabel, fontsize=15)
    axs[0].set_ylabel(ylabel, fontsize=15)
    axs[0].set_ylim((ymin,ymax))
    axs[0].set_yscale('linear')

    #axs1
    axs[1].hist(bdta[:-1], bdta, weights=pull.flatten(), color='green', range=(xmin, xmax))
    axs[1].hlines(-2.5, xmin, xmax, color='r', linestyles='--', alpha=0.5)
    axs[1].hlines( 2.5, xmin, xmax, color='r', linestyles='--', alpha=0.5)
    axs[1].hlines(-5.,  xmin, xmax, color='r', linestyles='-', alpha=0.5)
    axs[1].hlines( 5.,  xmin, xmax, color='r', linestyles='-', alpha=0.5)
    axs[1].set_ylabel('Pull')
    axs[1].set_xlim((xmin,xmax))
    axs[1].set_ylim((np.min(pull),np.max(pull)))

    fig.tight_layout()
    fig.savefig(fname)
    fig.clf()

def plot_1Dres(fit, bcntrs, nBins, Lmts, fname, xlabel, ylabel):
    from ROOT import TFile, TTree, TH1D, TH2D, gROOT, gStyle, TCanvas, kGreen, kRed, TCut, TPaveText, TLegend, kBlue, gPad
    gROOT.SetBatch(True)
    gStyle.SetOptStat(0);  
    gStyle.SetOptTitle(0);
    gROOT.ProcessLine(".x plots/lhcbStyle2D.C")

    hfit  = TH2D("hfit" , "hfit" , nBins[0], Lmts[0][0], Lmts[0][1], nBins[1], Lmts[1][0], Lmts[1][1])
    hfit.SetXTitle(xlabel); hfit.SetYTitle(ylabel); hfit.SetTitle("Fit"); hfit.SetZTitle("Events/Bin")
    for i, bcntr in enumerate(bcntrs):
        glbbin_f =  hfit.FindBin(bcntr[0], bcntr[1])
        hfit.SetBinContent(glbbin_f,   fit[i])

    c1 = TCanvas("c1","c1", 800, 400)
    hfit.Draw("colz")
    c1.SaveAs(fname)

def plot_2Dres(data, fit, bcntrs, nBins, Lmts, fname, xlabel, ylabel):
    data =  data.flatten()
    fit  =  fit.flatten()
    print(data.shape)
    print(fit.shape)

    print(fit.sum())
    fit = data.sum()/fit.sum() * fit #data unnormalised

    from ROOT import TFile, TTree, TH1D, TH2D, gROOT, gStyle, TCanvas, kGreen, kRed, TCut, TPaveText, TLegend, kBlue, gPad
    gROOT.SetBatch(True)
    gStyle.SetOptStat(0);  
    gStyle.SetOptTitle(0);
    gROOT.ProcessLine(".x plots/lhcbStyle2D.C")

    hdata = TH2D("hdata", "hdata", nBins[0], Lmts[0][0], Lmts[0][1], nBins[1], Lmts[1][0], Lmts[1][1])
    hdata.SetXTitle(xlabel); hdata.SetYTitle(ylabel); hdata.SetTitle("Data"); hdata.SetZTitle("Events/Bin")
    hfit  = TH2D("hfit" , "hfit" , nBins[0], Lmts[0][0], Lmts[0][1], nBins[1], Lmts[1][0], Lmts[1][1])
    hfit.SetXTitle(xlabel); hfit.SetYTitle(ylabel); hfit.SetTitle("Fit"); hfit.SetZTitle("Events/Bin")
    hpull  = TH2D("hpull" , "hpull" , nBins[0], Lmts[0][0], Lmts[0][1], nBins[1], Lmts[1][0], Lmts[1][1])
    hpull.SetXTitle(xlabel); hpull.SetYTitle(ylabel); hpull.SetTitle("Pull"); hpull.SetZTitle("Pull")
    for i, bcntr in enumerate(bcntrs):
        glbbin_d = hdata.FindBin(bcntr[0], bcntr[1])
        glbbin_f =  hfit.FindBin(bcntr[0], bcntr[1])
        glbbin_p = hpull.FindBin(bcntr[0], bcntr[1])
        hdata.SetBinContent(glbbin_d, data[i])
        hfit.SetBinContent(glbbin_f,   fit[i])
        pull_nm = (data[i] - fit[i])
        pull_dn = np.sqrt(data[i] + fit[i])
        if pull_dn == 0.:
            hpull.SetBinContent(glbbin_p,  0.)
        else:
            hpull.SetBinContent(glbbin_p,  pull_nm/pull_dn)

    #dnorm = hdata.Integral()
    #hfit.Scale(dnorm/hfit.Integral())

    c1 = TCanvas("c1","c1")
    hdata.Draw("colz")
    c1.SaveAs(fname.replace('.pdf', '_data.pdf'))

    c2 = TCanvas("c2","c2")
    hfit.Draw("colz")
    c2.SaveAs(fname.replace('.pdf', '_fit.pdf'))

    c3 = TCanvas("c3","c3")
    gStyle.SetPalette(70)
    hpull.Draw("colz")
    c3.SaveAs(fname.replace('.pdf', '_pull.pdf'))

def div_zn(a,b):
    return np.where(b==np.zeros_like(b, dtype=np.float64), b, a/b)

def ratio_err(a, b, deltaa, deltab): 
    return div_zn(a,b), np.sqrt(div_zn(deltaa**2,b**2) + div_zn(a**2*deltab**2,b**4))

def relratio_err(a, b, deltaa, deltab): 
    return div_zn(a-b,a),  np.sqrt((div_zn(np.array(1.),a) - div_zn((a-b), a**2))**2 * deltaa**2 + div_zn(deltab**2, a**2))

def get_normalized(dt, bdta):
    p_dta, p_dta_err= ratio_err(dt, np.array(dt.sum()), np.sqrt(dt), np.array(np.sqrt(dt.sum())))
    bw             = bdta[1:] - bdta[:-1]     
    bin_centers    = bdta[:-1] + 0.5 * bw
    return p_dta.flatten(), p_dta_err.flatten(), bin_centers.flatten()

def wc_randomise(wilcoefparam, Sess):
    cvl = np.random.uniform(2.*-0.07, 2.*0.07, size=1)[0]
    cvr = np.random.uniform(2.*-0.02, 2.*0.03, size=1)[0]
    csr = np.random.uniform(2.*-0.45, 2.*0.45, size=1)[0]
    csl = np.random.uniform(2.*-0.40, 2.*0.40, size=1)[0]
    ct  = np.random.uniform(2.*-0.07, 2.*0.07, size=1)[0]
    print(cvl,cvr,csr,csl,ct)
    wilcoefparam['V' ].update(Sess, (1.+cvl+cvr)) #1.-(1.+cvl+cvr)
    wilcoefparam['A' ].update(Sess, (1.+cvl-cvr)) #1.-(1.+cvl-cvr) 
    wilcoefparam['S' ].update(Sess, (csl+csr))
    wilcoefparam['PS'].update(Sess, (csl-csr))
    wilcoefparam['T' ].update(Sess, ct)
    wilcoefparam['PT'].update(Sess, ct)
    return None

def make_q2_cthl(dt_np, Lb4moml, Lc4moml, Mu4moml, Nu4moml):
    Lb4mom = LorentzVector(Vector(dt_np[Lb4moml[1]],dt_np[Lb4moml[2]],dt_np[Lb4moml[3]]), dt_np[Lb4moml[0]])
    Lc4mom = LorentzVector(Vector(dt_np[Lc4moml[1]],dt_np[Lc4moml[2]],dt_np[Lc4moml[3]]), dt_np[Lc4moml[0]])
    Mu4mom = LorentzVector(Vector(dt_np[Mu4moml[1]],dt_np[Mu4moml[2]],dt_np[Mu4moml[3]]), dt_np[Mu4moml[0]])
    Nu4mom = LorentzVector(Vector(dt_np[Nu4moml[1]],dt_np[Nu4moml[2]],dt_np[Nu4moml[3]]), dt_np[Nu4moml[0]])
    #Nu4mom = Nu4mom + (Lb4mom-(Lc4mom+Mu4mom+Nu4mom))
    #Boost Lb frame
    Lb4mom_Lb  = BoostToRest(Lb4mom, Lb4mom)
    Lc4mom_Lb  = BoostToRest(Lc4mom, Lb4mom)
    Mu4mom_Lb  = BoostToRest(Mu4mom, Lb4mom)
    Nu4mom_Lb  = BoostToRest(Nu4mom, Lb4mom)
    W4mom_Lb   = BoostToRest(Mu4mom+Nu4mom, Lb4mom)
    #Q2
    Q2_var     = tf.square(Mass(Mu4mom+Nu4mom))
    #Cthl
    Lb4mom_W  = BoostToRest(Lb4mom_Lb, W4mom_Lb)
    Mu4mom_W  = BoostToRest(Mu4mom_Lb, W4mom_Lb)
    Cthl_var  =-ScalarProduct(SpatialComponents(Mu4mom_W), SpatialComponents(Lb4mom_W))/(P(Mu4mom_W) * P(Lb4mom_W))
    #clthl    = ScalarProduct(SpatialComponents(Mu4mom_W), SpatialComponents(W4mom_Lb))/(P(Mu4mom_W) * P(W4mom_Lb))
    return Q2_var, Cthl_var

def HelAngles3Body(pa, pb, pc):
    theta_r  = Acos(-ZComponent(pc) / Norm(SpatialComponents(pc)))
    phi_r    = Atan2(-YComponent(pc), -XComponent(pc))
    pa_prime = RotateLorentzVector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = RotateLorentzVector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= BoostToRest(pa_prime, pa_prime+pb_prime)
    theta_a  = Acos(ZComponent(pa_prime2) / Norm(SpatialComponents(pa_prime2)))
    phi_a    = Atan2(YComponent(pa_prime2), XComponent(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = RotateLorentzVector(gm_p4mom_p1, -gm_phi_m, -Acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = RotateLorentzVector(gm_p4mom_p2, -gm_phi_m, -Acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = BoostToRest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = BoostToRest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2

def set_SM_vals(Wilcofparam, fFactparam, Sess):
    for k1 in list(Wilcofparam.keys()): 
        print('Setting', k1, 'to', 0.)
        Wilcofparam[k1].update(Sess, 0.)

    for k2 in list(fFactparam.keys()):
        print('Setting', k2, 'to', eff_ffact[k2][0])
        fFactparam[k2].update(Sess,  eff_ffact[k2][0])
            
    return None

def get_FP_Indp_terms(Lbampl, Wampl, densSM, sess = None):
    Weights = {}
    #THIS implementation is WAY better then tensor contraction in TF. Specially in terms of memory. 
    for WC in WCs:
        for FF in FFs:
            for WC_d in WCs:
                for FF_d in FFs:
                    fpindx   = (WC,   FF)
                    fpindx_d = (WC_d, FF_d)
                    dens = Const(0.)
                    for lLb in lLbs:
                        for lLc in lLcs:
                            for ll in lls:
                                for lw in lws:
                                    for lwd in lws:
                                        lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                        windx  = (lw,lwd,ReplaceWC(WC),ll)
                                        cond1  = lbindx not in Lbampl or windx not in Wampl
                                        if cond1: continue
                                        amp1  = sqrttwo*GF*Vcb*signWC[WC]*eta[lw]*eta[lwd]*Lbampl[lbindx]*Wampl[windx]
                                        for lLb_d in lLbs:
                                            for lLc_d in lLcs:
                                                for ll_d in lls:
                                                    for lw_d in lws:
                                                        for lwd_d in lws:
                                                            lbindx_d = (lLb_d,lLc_d,lw_d,lwd_d,WC_d,FF_d)
                                                            windx_d  = (lw_d,lwd_d,ReplaceWC(WC_d),ll_d)
                                                            cond2 = lbindx_d not in Lbampl or windx_d not in Wampl
                                                            cond3 = lLb_d != lLb or lLc_d != lLc or ll_d != ll
                                                            if cond2 or cond3: continue
                                                            amp2  = sqrttwo*GF*Vcb*signWC[WC_d]*eta[lw_d]*eta[lwd_d]*Lbampl[lbindx_d]*Wampl[windx_d]
                                                            dens += amp1 * amp2

                    Weights[(fpindx+fpindx_d)] = tf.where(tf.equal(densSM, 0.), Zeros(densSM), tf.divide(dens, densSM))

    return Weights

def contract_with_wcff(freeparams, indp_terms, sess = None):
    dynamic_dens = tf.add_n([freeparams[indx[0]] * freeparams[indx[1]] * indp_terms[indx] for indx in list(indp_terms.keys())])
    return dynamic_dens

def Indp_Integral(indp_terms, Phsp, lmts, x, mcsize, sess, niter = 1):
    org_lmts      = Phsp.ranges #old range (will set back to this range at the end)
    Phsp.ranges   = lmts        #new range, helps with sampling inside that limit
    volume = get_volume(Phsp.ranges)

    i = 0
    intg_terms = {}
    for indx in list(indp_terms.keys()):
        integral_val = []
        for i in range(niter):
            unifrom_smpl  = sess.run(Phsp.UnfilteredSampleGraph(size=mcsize))
            print(unifrom_smpl)
            print(volume * sess.run(indp_terms[indx], feed_dict={x, unifrom_smpl}))
            #integral_val += [sess.run(volume * tf.reduce_mean(indp_terms[indx]), feed_dict={x, unifrom_smpl})]
            #print(volume * sess.run(tf.reduce_mean(indp_terms[indx]), feed_dict={x, unifrom_smpl}))
            i += 1
            if i%10 == 0: print(i)

        intg_terms[indx] = sum(integral_val)/niter

    Phsp.ranges = org_lmts #new range, helps with sampling inside that limit
    return intg_terms

def get_FPIndp_terms(mdl, Phsp, lmts, ph, freeparams, switches, sess, mcsize = 100000, niter = 1):
    org_lmts      = Phsp.ranges #old range (will set back to this range at the end)
    Phsp.ranges   = lmts        #new range, helps with sampling inside that limit
    volume = get_volume(Phsp.ranges)
    #print(org_lmts)
    #print(Phsp.ranges)
    #print(volume)
    fddict = {}
    for ki in list(switches.keys()): fddict[switches[ki]] = [0.]
    fddict[ph]  = sess.run(Phsp.UnfilteredSampleGraph(size=mcsize)) #uniform sample within lmts provided
    #print(fddict[ph])

    def get_dens_term(key1, key2):
        fddict[switches[key1]] = [1.]
        fddict[switches[key2]] = [1.]
        #print(sess.run(mdl, feed_dict = fddict))
        #print(sess.run(volume * tf.reduce_mean(mdl), feed_dict = fddict))
        integral_val = volume * sess.run(tf.reduce_mean(mdl), feed_dict = fddict) #NB: IN summation over sample, the freeparams factor out (not done here)
        fddict[switches[key1]] = [0.]
        fddict[switches[key2]] = [0.]
        return integral_val

    fp_indp_terms = {}
    i = 0
    for k1 in list(switches.keys()):
        for k2 in list(switches.keys()):
            if k1 == k2: continue
            if (k2, k1) in list(fp_indp_terms.keys()): continue #NB: When switches s1 and s2 are ON they include both terms already (s1*s2*a1*conj_a2 + s2*s1*a2*conj_a1)
            #print(k1,k2)
            #(!) The 2nd term in 3 lines below should be conjugate however the free parameters are all real for us (!). 
            #fpsq1       = sess.run(freeparams[k1] * freeparams[k1]) 
            #fpsq2       = sess.run(freeparams[k2] * freeparams[k2])
            #fpsq12      = sess.run(freeparams[k1] * freeparams[k2])
            intg_mdl1   = get_dens_term(k1, k1)
            intg_mdl2   = get_dens_term(k2, k2)
            intg_mdl12  = get_dens_term(k1, k2)
            fp_indp_terms[(k1, k1)] = (intg_mdl1) #/fpsq1
            fp_indp_terms[(k2, k2)] = (intg_mdl2) #/fpsq2
            fp_indp_terms[(k1, k2)] = (intg_mdl12 - intg_mdl1 - intg_mdl2) #/fpsq12 #NB:The parameters need to included in the subrtraction
            i += 1
            if (i%10) == 0: print(i)

    Phsp.ranges = org_lmts #new range, helps with sampling inside that limit
    return fp_indp_terms

def get_binlimits(Binnames, Limits, q2bins, cthlbins):
    q2min       = Limits[0][0]
    q2max       = Limits[0][1]
    costhlmin   = Limits[1][0]
    costhlmax   = Limits[1][1]

    q2edges   = np.linspace(q2min, q2max, q2bins+1)
    cthledges = np.linspace(costhlmin, costhlmax, cthlbins+1)
    Binnum    = 0
    bin_limits= []
    for i in range(q2edges.shape[0]):
        for j in range(cthledges.shape[0]):
            if i == (q2edges.shape[0]-1) or j == (cthledges.shape[0]-1): break
            if 'Bin'+str(Binnum) in Binnames: bin_limits += [((q2edges[i], q2edges[i+1]), (cthledges[j], cthledges[j+1]))]
            Binnum+=1

    xmin = None; xmax = None; ymin = None; ymax = None
    for i, bin_limit in enumerate(bin_limits):
        if i == 0:
            xmin = bin_limit[0][0]
            xmax = bin_limit[0][1]
            ymin = bin_limit[1][0]
            ymax = bin_limit[1][1]
        else:
            if bin_limit[0][0] <= xmin: xmin = bin_limit[0][0] 
            if bin_limit[0][1] >= xmax: xmax = bin_limit[0][1] 
            if bin_limit[1][0] <= ymin: ymin = bin_limit[1][0] 
            if bin_limit[1][1] >= ymax: ymax = bin_limit[1][1] 

    return [(xmin, xmax), (ymin, ymax)]

def get_binlimits_new(Binname, q2edges, cthledges):
    Binnum    = 0
    bin_limits= []
    for i in range(q2edges.shape[0]):
        for j in range(cthledges.shape[0]):
            if i == (q2edges.shape[0]-1) or j == (cthledges.shape[0]-1): break
            if 'Bin'+str(Binnum) == Binname: bin_limits += [(q2edges[i], q2edges[i+1]), (cthledges[j], cthledges[j+1])]
            Binnum+=1

    return bin_limits

def get_binnames(lmts, Limits, q2bins, cthlbins):
    g_q2min     = lmts[0][0]
    g_q2max     = lmts[0][1]
    g_costhlmin = lmts[1][0]
    g_costhlmax = lmts[1][1]

    q2min       = Limits[0][0]; q2max = Limits[0][1]; costhlmin = Limits[1][0]; costhlmax = Limits[1][1]
    q2edges     = np.linspace(q2min, q2max, q2bins+1)
    cthledges   = np.linspace(costhlmin, costhlmax, cthlbins+1)
    Binnum      = 0
    included_bns= []
    for i in range(q2edges.shape[0]):
        for j in range(cthledges.shape[0]):
            if i == (q2edges.shape[0]-1) or j == (cthledges.shape[0]-1): break

            if q2edges[i] >= g_q2min and q2edges[i+1] <= g_q2max:
                if cthledges[j] >= g_costhlmin and cthledges[j+1] <= g_costhlmax:
                    included_bns += ['Bin'+str(Binnum)]

            Binnum+=1

    return included_bns

def get_volume(lmts):
    vols = []
    for lt in lmts:
        vol = 1.
        for r in lt: vol *= (r[1] - r[0])
        vols += [vol]

    return vols

def BinnedNLL(n_i, p_i, extended = False, sess = None):
    ##n_i is unnormalised and p_i is normalised
    n_tot = tf.reduce_sum(n_i)
    mu_i  = n_tot * p_i
    mu_tot= tf.reduce_sum(mu_i) #equal to n_tot
    if extended: mutot = FitParameter('mutot',ntot,-2.*ntot, 2.*ntot, 10.)
    nll   = mu_tot - n_tot - tf.reduce_sum(n_i * tf.log(mu_i))
    return nll

def check_symmetric(a, rtol=1e-05, atol=1e-08): return np.allclose(a, a.T, rtol=rtol, atol=atol)

def is_symm_pos_def(A):
    if check_symmetric(A):
        try:
            np.linalg.cholesky(A)
            return True
        except np.linalg.LinAlgError:
            print('here 2')
            return False
    else:
        return False

def get_FF_mean_cov(ff_names):
    #get mean
    mean = {}
    for l in open(fitdir+"/FF_cov/LambdabLambdac_results.dat", "r").readlines(): mean[l.split()[0]] = float(l.split()[1])
    mean_list = [mean[n] for n in ff_names]
    #get cov
    covariance = {}
    for l in list(mean.keys()): covariance[l] = {}
    for l in open(fitdir+"/FF_cov/LambdabLambdac_covariance.dat", "r").readlines(): covariance[l.split()[0]][l.split()[1]] = float(l.split()[2])
    cov_list = [[covariance[l1][l2] for l2 in ff_names] for l1 in ff_names]
    #convert to arrays
    mean_list = np.array(mean_list)
    cov_list  = np.array(cov_list)
    #check if symmetric and positive definite
    if not is_symm_pos_def(cov_list): 
        print('Not symmetric positive definite cov matrix, exiting')
        exit(0)
    else:
        print('Cov is symmetric and positive definite')

    return mean_list, cov_list

def GaussianConstraint(params, mean, cov): 
    const_dstb = tfd.MultivariateNormalFullCovariance(loc=mean, covariance_matrix=cov)
    return const_dstb.log_prob(params)

def SetParamValues(res, Sess, isfitresult = True):
    #for k in list(res.keys()):
    #    for ff1 in list(ffparam.keys()):
    #        if ff1 == k: 
    #            print('Setting', ff1, 'from old value of ', Sess.run(ffparam[ff1]), 'to a new value of ', res[k])
    #            if isfitresult:
    #                ffparam[ff1].update(Sess, res[k][0])
    #            else:
    #                ffparam[ff1].init_value = res[k]
    #                ffparam[ff1].update(Sess, res[k])
    #
    #    for wc1 in list(wcparam.keys()):
    #        if wc1 == k: 
    #            print('Setting', wc1, 'from old value of ', Sess.run(wcparam[wc1]), 'to a new value of ', res[k])
    #            if isfitresult:
    #                wcparam[wc1].update(Sess, res[k][0])
    #            else:
    #                wcparam[wc1].init_value = res[k]
    #                wcparam[wc1].update(Sess, res[k])

    for k in list(res.keys()):
        for p in tf.trainable_variables():
            if p.par_name == k:
                if isfitresult:
                    print('Setting', p.par_name, 'from ', Sess.run(p), 'to ', res[k][0])
                    p.update(Sess, res[k][0])
                else:
                    print('Setting', p.par_name, 'from ', Sess.run(p), 'to ', res[k])
                    p.init_value = res[k]
                    p.update(Sess, res[k])

    return None

def get_newbinning_info(bscheme, limits):
    nx_old, ny_old = 40, 40 
    oldbinvolume   = get_volume([limits])[0]/(nx_old*ny_old) #bin volume for 40 x 40 bins
    #new binning
    mx, my = None, None
    if bscheme == 'Bin40times40':
        mx, my = 1, 1
    elif bscheme == 'Bin20times20':
        mx, my = 2, 2
    elif bscheme == 'Bin10times10':
        mx, my = 4, 4
    elif bscheme == 'Bin5times5':
        mx, my = 8, 8
    
    nx_new, ny_new = int(nx_old/mx), int(ny_old/my)
    delta_x = (limits[0][1] - limits[0][0])/nx_new/2 #new delta_x for new binning with square bins
    delta_y = (limits[1][1] - limits[1][0])/ny_new/2 #new delta_y for new binning with square bins
    newbins      = []
    newbins_indxs= []
    newlmts      = []
    bincenters   = []
    for ic in range(int(nx_old/mx)):
        for ir in range(int(ny_old/my)):
            newbin_indx = []
            newbin      = []
            for ix in range((mx)):
                for iy in range((my)):
                    newbin_indx += [ir * my + (mx * ic + ix) * ny_old + iy]
                    newbin      += ['Bin'+str(newbin_indx[-1])]
    
            newlmt       = get_binlimits(newbin, limits, nx_old, ny_old)
            newbins      += [newbin]
            newbins_indxs+= [newbin_indx]
            newlmts      += [newlmt]
            bincenters   += [(newlmt[0][0] + delta_x, newlmt[1][0] + delta_y)]
            del newbin_indx, newbin, newlmt
    
    #print('newbins    '  , newbins)       #gives what bins in 40 x 40 bins lies in the new bin
    #print('newbins_indxs', newbins_indxs) #the same as above but only index
    #print('newlmts    '  , newlmts)       #gives limits of each bin [[(xminbin1, xmaxbin1),(yminbin1, ymaxbin1)],...]
    #print('bincenters '  , bincenters)    #gives bin centers of new binning scheme [(xcenterbin1, ycenterbin1),...]
    #print('len(newbins)    '  , len(newbins))
    #print('len(newbins_indxs)', len(newbins_indxs))
    #print('len(newlmts)    '  , len(newlmts))
    #print('len(bincenters) '  , len(bincenters))
    #print('oldbinvolume    '  , oldbinvolume)
    #print('nx_new, ny_new  '  , nx_new, ny_new )
    return newbins, newbins_indxs, newlmts, bincenters, oldbinvolume, nx_new, ny_new 


def get_model(newbins_indxs, oldbinvolume, floatedwc, nx, ny, applyEff = False, applyResponse = False):
    #get free parameter independent (nterms x nbins) matrix
    k_fpdinp  = pickle.load(open( fitdir+'/BinnedIndpTerms/BinIntegrals/keys1.p', 'rb')) #288 keys to map integral value with freeparams (wc,ff)
    print('len(k_fpdinp)'   , len(k_fpdinp))

    np_fpdinps=np.array([pickle.load(open(fitdir+'/BinnedIndpTerms/BinIntegrals/Bin'+str(i)+'.p','rb')) for i in range(40*40)]).T * oldbinvolume #integral values of shape (288, nbins)
    np_fpdinps=np.concatenate([np.sum(np_fpdinps[:, nindx], axis=1).reshape(-1,1) for nindx in newbins_indxs], axis=1)
    print('np_fpdinps.shape', np_fpdinps.shape)
    tf_fpdinps=Const(np_fpdinps)

    #get free params (fixing few things if necessary) and contracting
    Wilcoefparam = get_wilsonparams()
    for wcf in [p for p in tf.trainable_variables() if p.floating() if p.par_name not in floatedwc]: 
        print('Fixing', wcf.par_name)
        wcf.fix()

    FFactparam   = get_ffparams()
    fps, sws      = get_freeparams_dict(Wilcoefparam, FFactparam)
    tf_fps        = tf.reshape(tf.stack([fps[indx[0]] * fps[indx[1]] for indx in k_fpdinp]), [1,-1]) #2nd term should be conjugate, but they are real for us
    #build model
    Mdl_unnorm    = tf.reshape(tf.einsum('ij,jk->k', tf_fps, tf_fpdinps), (nx,ny)) #i=1,j=terms,k=nbins

    #fold efficiency: before normalising the pdf
    if applyEff:
        print('Apply eff')
        efftrue     = Const(pickle.load( open( fitdir+'/responsematrix_eff/Eff.p', 'rb' ) ))
        Mdl_unnorm *= efftrue

    #normalise model
    Mdl_intg      = tf.reduce_sum(Mdl_unnorm)
    Mdl_norm      = Mdl_unnorm/Mdl_intg

    #fold resolution: after normalising the pdf
    if applyResponse:
        print('Apply response matrix on normalised pdf')
        mijkl    = Const(pickle.load( open( fitdir+'/responsematrix_eff/responsematrix.p', 'rb' ))) #ijkl with ij (reco q2 and cthl) and kl (true q2 and cthl)
        Mdl_norm = tf.einsum('ijkl,kl->ij', mijkl, Mdl_norm) #should be normalised

    return Mdl_norm, fps, sws, FFactparam, Wilcoefparam
############################

############################
class MyPdf():
    def __init__(self, name, params):
        self.params = params

    def _unnormalized_pdf(self, x):
        #observables
        q2      = x[:,0]
        costhl  = x[:,1]
        #observables
        Vars = {}
        Vars['q2']      = q2
        Vars['costhl']  = costhl
        #Wilson coefficients 
        CVR = self.params['CVR'] 
        CVL = self.params['CVL'] 
        CSR = self.params['CSR']
        CSL = self.params['CSL']
        CT  = self.params['CT' ]  
        #Form factors
        FV1, FV2, FV3, FA1, FA2, FA3, fT, gT, fVT, gVT, fST, gST = self.GetFormFactors(q2)
        #Preliminary
        Q_p     = Mpos**2 - q2 
        Q_n     = Mneg**2 - q2 
        mag_p2  = tf.sqrt(Q_p * Q_n)/2./MLb
        #Helicity amplitudes: 
        #Note in the new paper: (Vector) f1 = FV1, f2 = -FV2/MLb and f3 = FV3/MLb and (Axial) same for g2, g2, g3
        #HV: 
        HV_ph_pt = (1. + CVL + CVR) * tf.sqrt(Q_p)/tf.sqrt(q2) * (Mneg * FV1 + q2/MLb * FV3)
        HV_ph_p  = (1. + CVL + CVR) * tf.sqrt(2 * Q_n) * (-FV1 - Mpos/MLb * FV2) #use the one in the new paper
        HV_ph_z  = (1. + CVL + CVR) * tf.sqrt(Q_n)/tf.sqrt(q2) * (Mpos * FV1 + q2/MLb * FV2)
        HV_nh_nt = HV_ph_pt
        HV_nh_n  = HV_ph_p
        HV_nh_z  = HV_ph_z
        #HA
        HA_ph_pt = (1. + CVL - CVR) * tf.sqrt(Q_n)/tf.sqrt(q2) * (Mpos * FA1 - q2/MLb * FA3)
        HA_ph_p  = (1. + CVL - CVR) * tf.sqrt(2 * Q_p) * (-FA1 + Mneg/MLb * FA2) #use the one in the new paper
        HA_ph_z  = (1. + CVL - CVR) * tf.sqrt(Q_p)/tf.sqrt(q2) * (Mneg * FA1 - q2/MLb * FA2)
        HA_nh_nt = - HA_ph_pt
        HA_nh_n  = - HA_ph_p
        HA_nh_z  = - HA_ph_z
        #H: HV - HA
        H_ph_pt = HV_ph_pt   -  HA_ph_pt
        H_ph_p  = HV_ph_p    -  HA_ph_p 
        H_ph_z  = HV_ph_z    -  HA_ph_z 
        H_nh_nt = HV_nh_nt   -  HA_nh_nt
        H_nh_n  = HV_nh_n    -  HA_nh_n 
        H_nh_z  = HV_nh_z    -  HA_nh_z 
        H_nh_pt = H_nh_nt #since t = 0 with JW = 0
        #HSP_x_y: Scalar - PsuedoScalar
        HSP_ph_z = (CSL + CSR) * tf.sqrt(Q_p)/(mb - mc) * (FV1 * Mneg + FV3 * q2/MLb) + (CSL - CSR) * tf.sqrt(Q_n)/(mb + mc) * (FA1 * Mpos - FA3 * q2/MLb)
        HSP_nh_z = (CSL + CSR) * tf.sqrt(Q_p)/(mb - mc) * (FV1 * Mneg + FV3 * q2/MLb) - (CSL - CSR) * tf.sqrt(Q_n)/(mb + mc) * (FA1 * Mpos - FA3 * q2/MLb)
        #HT_x_y_z: Tensor
        HT_ph_p_z   = -CT * tf.sqrt(2./q2) * (fT * tf.sqrt(Q_p) * Mneg + gT * tf.sqrt(Q_n) * Mpos)
        HT_ph_p_n   = -CT * (fT * tf.sqrt(Q_p) + gT * tf.sqrt(Q_n))
        HT_ph_p_pt  =  CT * (- tf.sqrt(2./q2) * (fT * tf.sqrt(Q_n) * Mpos + gT * tf.sqrt(Q_p) * Mneg) +  tf.sqrt(2.*q2) * (fVT * tf.sqrt(Q_n) - gVT * tf.sqrt(Q_p)))
        HT_ph_z_pt  =  CT * (- fT * tf.sqrt(Q_n) - gT * tf.sqrt(Q_p) + fVT * tf.sqrt(Q_n) * Mpos - gVT * tf.sqrt(Q_p) * Mneg + fST * tf.sqrt(Q_n) * Q_p + gST * tf.sqrt(Q_p) * Q_n )
        HT_nh_p_n   =  CT * (fT * tf.sqrt(Q_p) - gT * tf.sqrt(Q_n))
        HT_nh_z_n   =  CT * (tf.sqrt(2./q2) * (fT * tf.sqrt(Q_p) * Mneg - gT * tf.sqrt(Q_n) * Mpos))
        HT_nh_z_pt  =  CT * (-fT * tf.sqrt(Q_n) + gT * tf.sqrt(Q_p) + fVT * tf.sqrt(Q_n) * Mpos + gVT * tf.sqrt(Q_p) * Mneg + fST * tf.sqrt(Q_n) * Q_p - gST * tf.sqrt(Q_p) * Q_n)
        HT_nh_n_pt  =  CT * (-tf.sqrt(2./q2) * (fT * tf.sqrt(Q_n) * Mpos - gT * tf.sqrt(Q_p) * Mneg) + tf.sqrt(2. * q2) * (fVT * tf.sqrt(Q_n) + gVT * tf.sqrt(Q_p)))
        ##Relation: HT_L_l1_l2 =  - HT_L_l2_l1
        #HT_ph_z_p   = - HT_ph_p_z
        #HT_ph_n_p   = - HT_ph_p_n
        #HT_ph_pt_p  = - HT_ph_p_pt
        #HT_ph_pt_z  = - HT_ph_z_pt
        #HT_nh_n_p   = - HT_nh_p_n
        #HT_nh_n_z   = - HT_nh_z_n
        #HT_nh_pt_z  = - HT_nh_z_pt
        #HT_nh_pt_n  = - HT_nh_n_pt
        #N
        N = (GF**2 * Vcb**2 * q2 * mag_p2)/(512. * Pi()**3. * MLb**2) * (1 - Mlep**2/q2)**2
        #A1
        A1 = (2. * (1 - costhl**2) * (H_ph_z**2 + H_nh_z**2) + (1 - costhl)**2 * H_ph_p**2 + (1 + costhl)**2 * H_nh_n**2 )
        #AV2
        AV2  = (2. * costhl**2 * (H_ph_z**2 + H_nh_z**2) + (1 - costhl**2) * (H_ph_p**2 + H_nh_n**2) + 2 * (H_ph_pt**2 + H_nh_pt**2)) 
        AV2 -= (4 * costhl * (H_ph_z * H_ph_pt + H_nh_z * H_nh_pt))
        #AT2
        AT2  = 1./4. * (2. * (1 - costhl**2) * (HT_ph_p_n**2 + HT_ph_z_pt**2 + HT_nh_p_n**2 + HT_nh_z_pt**2 + 2. * HT_ph_p_n * HT_ph_z_pt + 2. * HT_nh_p_n * HT_nh_z_pt))
        AT2 += 1./4. * ((1 + costhl)**2 * (HT_nh_z_n**2 + HT_nh_n_pt**2 + 2. * HT_nh_z_n * HT_nh_n_pt)) 
        AT2 += 1./4. * ((1 - costhl)**2 * (HT_ph_p_z**2 + HT_ph_p_pt**2 + 2. * HT_ph_p_z * HT_ph_p_pt)) 
        #A3
        A3  = 1./8. * (2. * costhl**2 * (HT_ph_p_n**2 + HT_ph_z_pt**2 + HT_nh_p_n**2 + HT_nh_z_pt**2 + 2. * HT_ph_p_n * HT_ph_z_pt + 2. * HT_nh_p_n * HT_nh_z_pt))
        A3 += 1./8. * ((1 - costhl**2) * (HT_ph_p_z**2 + HT_ph_p_pt**2 + HT_nh_z_n**2 + HT_nh_n_pt**2 + 2. * HT_ph_p_z * HT_ph_p_pt + 2. * HT_nh_z_n * HT_nh_n_pt))
        A3 += (HSP_ph_z**2 + HSP_nh_z**2)
        #A4
        A4  = (-costhl * (H_ph_z * HSP_ph_z + H_nh_z * HSP_nh_z) + (H_ph_pt * HSP_ph_z + H_nh_pt * HSP_nh_z))
        A4 += (costhl**2/2. * (H_ph_z * HT_ph_p_n + H_ph_z * HT_ph_z_pt + H_nh_z * HT_nh_p_n + H_nh_z * HT_nh_z_pt))
        A4 -= (costhl/2. * (H_ph_pt * HT_ph_p_n + H_ph_pt * HT_ph_z_pt + H_nh_pt * HT_nh_p_n + H_nh_pt * HT_nh_z_pt)) 
        A4 += ((1. - costhl)**2/4. * (H_ph_p * HT_ph_p_z + H_ph_p * HT_ph_p_pt))
        A4 += ((1. + costhl)**2/4. * (H_nh_n * HT_nh_z_n + H_nh_n * HT_nh_n_pt))
        A4 += ((1. - costhl**2)/4. * (H_ph_p * HT_ph_p_z + H_ph_p * HT_ph_p_pt + H_nh_n * HT_nh_z_n + H_nh_n * HT_nh_n_pt + 2. * H_ph_z * HT_ph_p_n + 2. * H_ph_z * HT_ph_z_pt + 2. * H_nh_z * HT_nh_p_n + 2. * H_nh_z * HT_nh_z_pt))
        #A5
        A5 = -2. * costhl * (HSP_ph_z * HT_ph_p_n + HSP_ph_z * HT_ph_z_pt + HSP_nh_z * HT_nh_p_n + HSP_nh_z * HT_nh_z_pt)
        #diff density
        dG_dq2_dcosthl =  N * (A1 + Mlep**2/q2 * (AV2 + AT2) + 2. * A3 + 4. * Mlep/tf.sqrt(q2) * A4 + A5)
        return dG_dq2_dcosthl , Vars

    def GetFormFactors(self, q2):
        #define delataf
        Deltaf = {}
        Deltaf['fplus']  = Const(56e-3)  #GeV
        Deltaf['fperp']  = Const(56e-3)  #GeV
        Deltaf['f0']     = Const(449e-3) #GeV
        Deltaf['gplus']  = Const(492e-3) #GeV
        Deltaf['gperp']  = Const(492e-3) #GeV
        Deltaf['g0']     = Const(0.)     #GeV
        M_Bc = tf.constant(6.276, dtype = tf.float64) #GeV, from straub paper
        #define mf_pole, tf_p, zf, Weinberg form factor functions
        mf_pole = lambda deltaf: M_Bc + deltaf 
        tf_plus = lambda Mf_pole: Mf_pole**2
        zf = lambda Q2, Tf_p: (tf.sqrt(Tf_p - Q2) - tf.sqrt(Tf_p - t0))/(tf.sqrt(Tf_p - Q2) + tf.sqrt(Tf_p - t0))
        ff = lambda Q2, Mf_pole, A0, A1: 1./(1. - Q2/Mf_pole**2) * (A0 + A1 * zf(Q2, tf_plus(Mf_pole)))
        #Weinber FF
        fplus = ff(q2, mf_pole(Deltaf['fplus']), self.params['a0fplus'], self.params['a1fplus'])
        fperp = ff(q2, mf_pole(Deltaf['fperp']), self.params['a0fperp'], self.params['a1fperp'])
        f0    = ff(q2, mf_pole(Deltaf['f0']),    self.params['a0f0']   , self.params['a1f0']   )
        gplus = ff(q2, mf_pole(Deltaf['gplus']), self.params['a0gplus'], self.params['a1gplus'])
        gperp = ff(q2, mf_pole(Deltaf['gperp']), self.params['a0gperp'], self.params['a1gperp'])
        g0    = ff(q2, mf_pole(Deltaf['g0']),    self.params['a0g0']   , self.params['a1g0']   )
        #Do the transformation Weinber -> Helicity ff
        #functions to transform Weinberg ff -> helicity ff
        f2 = lambda Fplus, Fperp, A, B: (Fplus - Fperp)/(A - B)
        f1 = lambda Fperp, B, F2: Fperp - B * F2
        f3 = lambda F0, F1, C: (F0 - F1)/C
        #Helicity ff
        af  = q2/MLb/Mpos
        bf  = Mpos/MLb
        cf  = q2/MLb/Mneg
        ag  = -q2/MLb/Mneg
        bg  = -Mneg/MLb
        cg  = -q2/MLb/Mpos
        FV2 = f2(fplus, fperp, af, bf)
        FV1 = f1(fperp, bf, FV2)
        FV3 = f3(f0, FV1, cf)
        FA2 = f2(gplus, gperp, ag, bg)
        FA1 = f1(gperp, bg, FA2)
        FA3 = f3(g0, FA1, cg)
        #HQET relations for (pseudo-)tensor form factors
        fT  = FV1
        gT  = FV1
        fVT = 0.
        gVT = fVT
        fST = fVT
        gST = fVT
        return (FV1, FV2, FV3, FA1, FA2, FA3, fT, gT, fVT, gVT, fST, gST)
############################

############################
def integral(pdf = None, ranges = None): 
    return tf.reduce_mean(pdf) * functools.reduce(lambda x, y: x*y, [r[1] - r[0] for r in ranges])
    
class PDF_TFGraph:
    """
    Class to define mainly the Tensorflow graph of normalised pdf
    """

    def __init__(self, phsp_limits):
        """
        Default initialisation function

        Take as input 
        -------------
        
        phsp_limits: Limits of q^2 and cos(theta_mu). It should be for the form [(qsq_min, qsq_max),(costh_min, costh_max)]

        Define member variables (cache):
        -------------------------------

        self.pdf      : Tensorflow graph of normalised pdf.
        self.phsp     :  Rectangular phase space of qsq and costhmu
        self.norm_smpl: Uniform normalisation sample to be used later to integrate the pdf
        """

        #get wilson coefficients (wc)
        wc_params     = get_wilsonparams()
        #get form factor (ff) parameters
        ff_params     = get_ffparams()
        #multiply wc and ff
        wc_times_ff, swts  = get_freeparams_dict(wc_params, ff_params)

        #define rectangular phase space with limits i.e. (qsq, costhmu) space and define place holders for data and normalisation sample
        self.phsp          = RectangularPhaseSpace(ranges=phsp_limits)
        data_ph            = self.phsp.data_placeholder
        norm_ph            = self.phsp.norm_placeholder

        #generate an using numpy uniform normalisation sample to be used later to normalise the pdf function
        self.norm_smpl     = self.phsp.UnfilteredSample(1000000)
        #define tf graph of unnormalised pdf
        pdf_unnorm         = AngularModel(data_ph, wc_times_ff, swts)[0]
        #define tf graph of pdf integral
        pdf_intg           = integral(AngularModel(norm_ph, wc_times_ff, swts)[0], ranges = phsp_limits)
        #define tf graph of normalised pdf
        self.pdf           = pdf_unnorm/pdf_intg

    def values(self, wc_name, wc_val, phsp_array, sess):
        """
        Function that retuns the pdf values given array of q^2 and cos(thetamu) and a new value of wilson coefficient
    
        Input
        -----
    
        wc_name    : Name of the wilson coefficient whose value you want to change. Options are CVR, CSR, CSL and CT.
        wc_val     : New value of the wilson coefficient. Note this should just be a scalar number NOT an array.
        phsp_array : This is the array of q^2 and costh_mu. The shape of the array should be (rows x columns) = (n, 2). 
                     The 1st columns MUST be q^2 in GeV^2, the 2nd column MUST be cos(theta_mu) and n denotes the sample size. 
                     E.g. phsp_array = [[0.1, 0.2], 
                                        [1.3, 0.1], 
                                        [5.3, -0.2]] 
                     where the shape array is (3,2)
        sess       : A tensorflow session. Note the global variables need to be normalised before hand.
    
        Output
        ------
    
        Returns values of the pdf at the phase space points for the new value of wilson coeffficient
        If the shape of the phsp_array is (rows x columns) = (n,2) then the output shape is (n,).
        """

        #get the previous value of wc
        wc_prev_val = None
        for wcf in [p for p in tf.trainable_variables() if p.floating() if p.par_name == wc_name]:wc_prev_val = sess.run(wcf)

        #set a new value for wc
        print('Setting a new value')
        SetParamValues({wc_name: wc_val}, sess, isfitresult = False)
    
        #get the values of the normalised pdf
        pdf_vals  = sess.run(self.pdf, feed_dict = {self.phsp.data_placeholder: phsp_array, self.phsp.norm_placeholder: self.norm_smpl})
    
        #set the old value of wc back
        print('Setting back the old value')
        SetParamValues({wc_name: wc_prev_val}, sess, isfitresult = False)

        return pdf_vals
############################
