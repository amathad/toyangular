###################
#Inport tensorflow (TF) related things
import os, sys
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisNew")
import tensorflow as tf
config  = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
from TensorFlowAnalysis import *
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
#Import global vars and fit models from util.py
sys.path.append(os.path.abspath(os.getcwd()))
from util import * 
###################

def main():
    #define phase space (phsp) limits
    qsq_min   = Mlep**2
    qsq_max   = (MLb - MLc)**2
    costh_min = -1.
    costh_max =  1.
    limts     = [(qsq_min, qsq_max), (costh_min, costh_max)] 
    print(limts)
    
    #defint the tensorflow graph of normalised pdf
    pdf_tfgrph =  PDF_TFGraph(limts)
    
    #TODO: 
    #Make the phase space array or the meshgrid of q^2 and costh_mu
    #Here as an example, I have made three data points so the shape of the array is (row x columns) = (3, 2)
    #Note the first columns MUST be qsq in Gev^2 and second column MUST be cos(theta_mu)
    phsp_arr = np.array([[0.1, 0.2], 
                         [1.3, 0.1],
                         [5.3,-0.1]])
    print(phsp_arr.shape)
    
    #define a session
    with tf.Session(config=config) as sess:
        #set initial value for the variables. SM values are set for Wilson coefficents and form factors
        sess.run(tf.global_variables_initializer())
    
        #get the SM pdf values at the phase space array setting CVR to zero
        wcname    = "CVR"
        wcnewval  = 0.00  #set CVR to zero
        pdf_sm    = pdf_tfgrph.values(wcname, wcnewval, phsp_arr, sess)
        print('Standard Model (SM)', pdf_sm)

        #get the pdf values for various values of CVR and take difference with respect to SM value
        wcnewvals  = [0.01,-0.02]  #TODO: Replace this with sampling of CVR from gaussian of mean of 0.0 and width of 0.03. Here I have made a list of two random values as example.
        diff_vals  = []
        for wcnewval in wcnewvals:
            #get the new values of pdf when CVR is nonzero
            pdf_np   = pdf_tfgrph.values(wcname, wcnewval, phsp_arr, sess)
            print('New Physics (NP)', pdf_np)
            #Take the absolute value of the (SM - NP) difference
            diff_val = np.abs(pdf_sm - pdf_np) 
            print('Abs(SM - NP)', diff_val)
            #put these values in a list which can later me used to get the maximum of these differences at a given q^2 and cos(theta_mu) point
            diff_vals += [diff_val]

        print(len(diff_vals)) #Since we have used two values of CVR this should be 2.

        #TODO: 
        #Get the maximum at deviation at a given q^2 and costhmu point
        #Hint: You can turn the list 'diff_vals' value into a numpy array of shape (row x columns) = (data_size x cvr_values) 
        #      where data_size is the rows of the phsp_arr and cvr_values is the number of CVR values that you have generated. 
        #      Then you should be able to use a numpy 'max' function to get the maximul deviation (See https://numpy.org/doc/stable/reference/generated/numpy.ndarray.max.html)

        #TODO
        #Plot the maximum deviation as function of q^2 and cos(theta_mu) using matplotlib

if __name__ == '__main__':
    main()
