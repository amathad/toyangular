###################
#Import things
import tensorflow as tf
import sys, os
import argparse
#import tracemalloc
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisNew")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
config  = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
from TensorFlowAnalysis import *
import time
#Import global vars and fit models from util.py
sys.path.append(os.path.abspath("../"))
from util import * 
sys.path.append(os.path.abspath(os.getcwd()))
from Binning_Scheme import BinScheme
###################

def main():
    ###########- configure
    binname = 'Bin'+str(binnum)
    seed    = binnum+10
    tf.random.set_random_seed(seed)
    np.random.seed(seed)
    ############
    
    ############
    limits  = [(Mlep**2, (MLb - MLc)**2)]
    limits += [(-1., 1.)]
    print(limits)
    phsp     = RectangularPhaseSpace(ranges=limits)
    norm_ph  = phsp.norm_placeholder
    fpdinp   = get_FPIndp_terms_test(norm_ph)
    ks, vs   = list(fpdinp.keys()), list(fpdinp.values())
    print(len(ks))
    savedir = os.path.abspath(os.getcwd()+'/'+scheme)
    if binname == 'Bin0': pickle.dump( ks, open(savedir+'/keys'+str(binnum)+'.p', "wb" ))
    ###########
    ###########
    bin_lmts    = get_binlimits_new(binname, np.array(BinScheme[scheme]['qsq']), np.array(BinScheme[scheme]['cthl']))
    phsp.ranges = bin_lmts
    print(phsp.ranges)
    with tf.Session(config=config) as sess:
        result  = sess.run(vs, feed_dict = {norm_ph: sess.run(phsp.UnfilteredSampleGraph(size=1000000))})
        pickle.dump( result, open(savedir+'/Bin'+str(binnum)+'.p', "wb" ))
    ##############
    
    ############### Test 1 - Actual density values
    #limits  = [(Mlep**2, (MLb - MLc)**2)]
    #limits += [(-1., 1.)]
    #phsp                = RectangularPhaseSpace(ranges=limits)
    #norm_ph             = phsp.norm_placeholder
    #Wilcoef_delta       = get_wilsonparams()
    #FFact_delta         = get_ffparams()
    #fps, sws            = get_freeparams_dict(Wilcoef_delta, FFact_delta)
    #
    #fpdinp              = get_FPIndp_terms_test(norm_ph) #NB: You have to change this function to give density rather than integral
    #model_1             = contract_with_wcff(fps, fpdinp)
    #model_2             = AngularModel(norm_ph, fps, sws)
    #sess                = tf.Session(config=config)
    #sess.run(tf.global_variables_initializer())
    #norm_smpl           = sess.run(phsp.UnfilteredSampleGraph(size=10))
    #print(sess.run(model_1, feed_dict = {norm_ph: norm_smpl}))
    #print(sess.run(model_2[0], feed_dict = {norm_ph: norm_smpl}))
    ###############
    
    ################ Test 2 - Actual integral values in a given bin
    #limits  = [(Mlep**2, (MLb - MLc)**2)]
    #limits += [(-1., 1.)]
    #phsp                = RectangularPhaseSpace(ranges=limits)
    #bin_lmts            = get_binlimits_new(binname, np.array(BinScheme[scheme]['qsq']), np.array(BinScheme[scheme]['cthl']))
    #phsp.ranges         = bin_lmts
    #print(phsp.ranges)
    #norm_ph             = phsp.norm_placeholder
    #Wilcoef_delta       = get_wilsonparams()
    #FFact_delta         = get_ffparams()
    #fps, sws            = get_freeparams_dict(Wilcoef_delta, FFact_delta)
    #
    #fpdinp              = get_FPIndp_terms_test(norm_ph)
    #model_1             = contract_with_wcff(fps, fpdinp) #in now integral
    #model_2             = tf.reduce_mean(AngularModel(norm_ph, fps, sws)[0]) #in now integral
    #sess                = tf.Session(config=config)
    #sess.run(tf.global_variables_initializer())
    #norm_smpl           = sess.run(phsp.UnfilteredSampleGraph(size=1000000))
    #print(sess.run(model_1, feed_dict = {norm_ph: norm_smpl}))
    #print(sess.run(model_2, feed_dict = {norm_ph: norm_smpl}))
    ################
    
    ################# Test 2 - Import the cached integral and check against 
    #limits  = [(Mlep**2, (MLb - MLc)**2)]
    #limits += [(-1., 1.)]
    #savedir   = os.path.abspath(os.getcwd()+'/'+scheme)
    #np_fpdinps= pickle.load(open(savedir+'/Bin'+str(binnum)+'.p','rb'))
    #k_fpdinp  = pickle.load(open(savedir+'/keys0.p', 'rb'))
    #print(len(np_fpdinps))
    #print(len(k_fpdinp))
    #fpdinp    = dict(zip(k_fpdinp,np_fpdinps))
    #
    #phsp                = RectangularPhaseSpace(ranges=limits)
    #bin_lmts            = get_binlimits_new(binname, np.array(BinScheme[scheme]['qsq']), np.array(BinScheme[scheme]['cthl']))
    #phsp.ranges         = bin_lmts
    #print(phsp.ranges)
    #norm_ph             = phsp.norm_placeholder
    #Wilcoef_delta       = get_wilsonparams()
    #FFact_delta         = get_ffparams()
    #fps, sws            = get_freeparams_dict(Wilcoef_delta, FFact_delta)
    #
    #model_1             = contract_with_wcff(fps, fpdinp) #in now integral
    #model_2             = tf.reduce_mean(AngularModel(norm_ph, fps, sws)[0]) #in now integral
    #sess                = tf.Session(config=config)
    #sess.run(tf.global_variables_initializer())
    #norm_smpl           = sess.run(phsp.UnfilteredSampleGraph(size=1000000))
    #print(sess.run(model_1))
    #print(sess.run(model_2, feed_dict = {norm_ph: norm_smpl}))
    #################
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Arguments for Cache_Integrals.py')
    parser.add_argument('-s', '--scheme', dest='scheme',type=str, required=True, help='(string) The binning scheme to use. This scheme needs to have implemented in Binning_Schemes.py file. Available options are Scheme{0,1,2,3,4}.')
    parser.add_argument('-b', '--binnum', dest='binnum',type=int,required=True, help='(string) Either a list or just one bin number from the scheme. User needs to know appriorihow many bins are there in the scheme (See Binning_Schemes.py).')
    args       = parser.parse_args()
    scheme     = args.scheme
    binnum     = args.binnum
    if scheme not in list(BinScheme.keys()): 
        raise Exception('The specified binning scheme does not exist!')

    total_bins = (len(BinScheme[scheme]['qsq']) - 1) * (len(BinScheme[scheme]['cthl']) - 1)
    if binnum >= total_bins:
        raise Exception('The specified bin number does not exist')

    print(args)
    #tracemalloc.start()
    main()
    #current, peak = tracemalloc.get_traced_memory()
    #print(f"Current memory usage is {current / 10**6} MB; Peak was {peak / 10**6} MB")
    #tracemalloc.stop()
