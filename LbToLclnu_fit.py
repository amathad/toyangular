###################
#Import standard things
import sys, os
import time, pprint
import argparse
#Import tensorflow (TF) related things
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisNew")
import tensorflow as tf
import tensorflow_probability as tfp
config  = tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1)
from TensorFlowAnalysis import *
tfd = tfp.distributions
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
#Import global vars and fit models from util.py
sys.path.append(os.path.abspath(os.getcwd()))
from util import * 
###################

def main():
    start = time.time()

    ###################
    """Make the FF list to exclude and set the randomisation algorithm seed"""
    tf.random.set_random_seed(seed)
    np.random.seed(seed)
    ###################
    
    ####################
    """Set limits and move from the original coarse binning of 40 x 40 scheme to 5 x 5 scheme"""
    limits  = [(Mlep**2, (MLb - MLc)**2)]; 
    limits += [(-1., 1.)]
    newbins, newbins_indxs, newlmts, bincenters, oldbinvolume, nx_new, ny_new  = get_newbinning_info(bscheme, limits)
    #print(newbins)
    #print(newbins_indxs)
    #print(newlmts)
    #print(bincenters)
    ####################
    
    ############ 
    """Define Model
    The function returns 
        - Mdl_norm: normalised model, 
        - fps: free parameters i.e. form factor (FF) times wilson coefficient (WC). Ignore them they are only used when generating unbinned sample. We are not doing that here.
        - sws: Switches. Ignore them they are only used when generating unbinned sample. We are not doing that here.
        - FFact: Map of FF name -> FF parameter
        - Wilcoef: Map of operators names -> WC parameters
    """
    Mdl_norm, fps, sws, FFact, Wilcoef = get_model(newbins_indxs, oldbinvolume, floatWC, nx_new, ny_new, applyEff = effn, applyResponse = resn)
    ############
    
    ############ 
    """Define TF session and initialise the variables"""
    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())
    ############

    ###############
    """Generate binned toy/fake data with the initialised values"""
    pois     = tfd.Poisson(rate = nevntsfact * 7.5e6 * Mdl_norm)
    b_data   = sess.run(pois.sample(1, seed=seed+20)[0,:])
    print('b_data.shape'  , b_data.shape)
    print('np.sum(b_data)', np.sum(b_data))
    ##############- Get true unbinned data and bin it
    #phsp       = RectangularPhaseSpace(ranges=limits)
    #norm_ph    = phsp.norm_placeholder
    #Model,obsv = AngularModel(norm_ph, fps, sws)
    #norm_smpl  = sess.run(phsp.UnfilteredSampleGraph(size=1000000))
    #data       = genmodel(sess, Model, norm_ph, norm_smpl, phsp, Seed=int(seed)+20, Size=int(nevntsfact * 10000.), Obsv = obsv, fname = 'test.root')
    #print('data.shape', data.shape)
    #b_data     = np.float64(np.histogram2d(data[:,0], data[:,1], bins=(nx_new, ny_new), range=limits)) 
    #############
    
    ##############
    """Fix or float the form factors as required"""
    for p in tf.trainable_variables():
        if 'a' in p.par_name: 
            print('Fixing', p.par_name)
            p.fix()
            for exc in floated_FF:
                if exc in p.par_name: 
                    print('Floating', p.par_name)
                    p.float()
    
    #Make a list of parameters to randomise at each minimisation step.
    wc_floats = [p for p in tf.trainable_variables() if p.floating() if 'C' in p.par_name] 
    ff_floats = [p for p in tf.trainable_variables() if p.floating() if 'a' in p.par_name] 
    ##############

    ############## - 
    """Build the NLL and Gaussian constrain FF
        To get good coverage modify central value of the Gaussian constraint 
    """
    NLL  = BinnedNLL(Const(b_data), Mdl_norm, sess = sess)
    if len(ff_floats) != 0:
        print('Constraining form factors')
        ff_names = [p.par_name for p in ff_floats]
        ff_mean, ff_cov = get_FF_mean_cov(ff_names)
        ff_mean_mod     = np.random.multivariate_normal(mean=ff_mean, cov=ff_cov) 
        print('Modified', ff_mean, 'to', ff_mean_mod, 'for', ff_names)
        NLL       -= GaussianConstraint(ff_floats, ff_mean_mod, ff_cov)  #Ltot = L * Lg => NLLtot = NLL - LogLg

    print('True NLL', sess.run(NLL, feed_dict=None))
    ##############

    ##############
    """Conduct the fits. The fits are conducted nfits times to the fake data"""
    nllval = None; reslts = None
    nfits  = 20 
    for nfit in range(nfits):
        #Randomise WCs the params: FF are randomised according to Gaussian and WC are randomised according to uniform distribution
        for wc_parm in wc_floats:
            print('Before randomising WC: ', wc_parm.par_name, sess.run(wc_parm))
            wc_parm.randomise(sess)
            print('After randomising WC: ', wc_parm.par_name, sess.run(wc_parm))

        #Randomise FF according to LQCD gaussian distribution
        if len(ff_floats) != 0:
            ff_mod  = np.random.multivariate_normal(mean=ff_mean, cov=ff_cov) 
            for ffp, ffval in zip(ff_floats, ff_mod):
                print('Before randomising FF: ', ffp.par_name, sess.run(ffp))
                ffp.init_value = ffval
                ffp.update(sess, ffval)
                print('After randomising FF: ', ffp.par_name, sess.run(ffp))

        #Conduct the fit
        results = RunMinuit(sess, NLL, feed_dict=None, call_limit=50000, useGradient=False, printout = 3000, runHesse=True, runMinos=False)

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            print('Fit number', nfit)
            nllval = results['loglh']
            reslts = results
        else:
            print('Fit number', nfit)
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results
    
    #set the parameters to the fit results of the least NLL
    SetParamValues(reslts, sess)
    pprint.pprint(reslts)
    #write the fit results
    resname = 'results_'+floatWC+'_'+str(seed)+'_'+bscheme+'_'+str(nevntsfact)+'_'+'_'.join(floated_FF)+'_'+suffix
    resfname = direc+resname+'.txt'
    print('resfname', resfname)
    b_fit = sess.run(Mdl_norm)
    WriteFitResults(reslts, resfname)
    #plot the fit results
    plotfname = direc+resname+'.pdf'
    if plotRes: plot_2Dres(b_data, b_fit, bincenters, (nx_new, ny_new), limits, plotfname, "q^{2} [GeV^{2}]", "cos(#theta_{#mu})")
    
    end = time.time(); print('Time taken in min', (end - start)/60.)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Arguments for LbToLclnu_fit.py')
    #required aguments
    parser.add_argument('-f', '--floatWC'   , dest='floatWC',type=str, required=True, help='(string) Name of the Wilson coefficient (WC) to be floated. Available options are [CVR,CSR,CSL,CT].')
    parser.add_argument('-s', '--seed'      , dest='seed'   ,type=int, required=True, help='(int) Seed for generation of fake/toy data. This should be different for each toy.')
    #optional arguments
    parser.add_argument('-b', '--bscheme'   , dest='bscheme',type=str, default='Bin5times5',help='(string) Binning scheme to be used. Available options are [Bin40times40,Bin20times20,Bin10times10,Bin5times5] and default is Bin5times5.')
    parser.add_argument('-n', '--nevntsfact', dest='nevntsfact',type=int, default=1,help='(int) Each toy is generated with 7.5M events, this sets a factor to scale that number down or up. Default is 1.')
    parser.add_argument('-d', '--direc'     , dest='direc',type=str, default='./plots/',help='(string) Directory in which the fit result (.txt) and plot is to be saved. Default in current directory.')
    parser.add_argument('-sf', '--suffix'    , dest='suffix',type=str, default='toy',help="(int) A unique suffix added to the name of the fit result file (*_suffix.txt) and plot file (*_suffix.pdf). Default is 'toy'.")
    parser.add_argument('-p', '--plotRes'   , dest='plotRes',type=str2bool,default='True',help='(bool) Set to False if you do not want to plot the result. Default is True.')
    parser.add_argument('-effn', '--effn'   , dest='effn',type=str2bool,default='True',help='(bool) Set to False if you do not want efficiency included in model. Default is True.')
    parser.add_argument('-resn', '--resn'   , dest='resn',type=str2bool,default='True',help='(bool) Set to False if you do not want resolution information included in model. Default is True.')
    parser.add_argument('-e', '--floated_FF' , dest='floated_FF',nargs='+', default = ['None'], 
    help="(list) List of form factor (FF) parameters that you want floated in the fit. \
          Default is 'None' that is all FF parameters are fixed. \
          When CVR or CSR or CSL is set as 'floatWC': 11 FF parameters can be floated, they are a0f0 a0fplus a0fperp a1f0 a1fplus a1fperp a0g0 a0gplus a1g0 a1gplus a1gperp \
          When CT is set as 'floatWC': In addition to the 11 FF, we can fix 7 more which are a0hplus a0hperp a0htildeplus a1hplus a1hperp a1htildeplus a1htildeperp") 
    args       = parser.parse_args()
    floatWC    = args.floatWC
    seed       = args.seed
    bscheme    = args.bscheme
    nevntsfact = args.nevntsfact
    suffix     = args.suffix
    direc      = args.direc
    plotRes    = args.plotRes
    floated_FF  = args.floated_FF
    effn       = args.effn
    resn       = args.resn
    print(args)
    if not direc.endswith('/'): direc += '/'
    main()
